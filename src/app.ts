import express from 'express'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import {rootRoute} from './configuration/REST/root.route'

const cors = require('cors')

dotenv.config({path: process.env.ENVFILE || '.env.inmemory'})

const app = express()

app.set('port', process.env.PORT || 8000)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(cors())

rootRoute(app)

export default app
