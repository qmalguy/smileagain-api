import {Request, Response} from "express";
import {map, head} from 'ramda'
import {UserHandler} from "../../../usecases/user.handler";
import {UserPresenter} from "../../presenters/UserPresenter.present";
import {GetUserById} from "../../../usecases/queries/getUserById.query";

export const getUserById = (userHandler: UserHandler) => (req: Request, res: Response) => {
    try {
        const userPresented = head(map(UserPresenter.present, userHandler.query(new GetUserById(req.params.id))))
        res.send(userPresented)
    } catch (e) {
        res.status(404).send(e.message)
    }
}