import {Request, Response} from "express";
import {map} from 'ramda'
import {UserHandler} from "../../../usecases/user.handler";
import {UserPresenter} from "../../presenters/UserPresenter.present";
import {FetchAllUsers} from "../../../usecases/queries/fetchAllUsers.query";

export const fetchAllUsers = (userHandler: UserHandler) => (req: Request, res: Response) => {
    const UsersPresented = map(
        UserPresenter.present,
        userHandler.query(new FetchAllUsers())
    );

    res.send(UsersPresented)
}