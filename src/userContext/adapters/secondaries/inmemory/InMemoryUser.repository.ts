import {User} from "../../../domains/entities/user";
import {filter, propEq} from 'ramda'
import {UserRepository} from "../../../domains/repositories/user.repository";

export class InMemoryUserRepository implements UserRepository {
    constructor(private users: User[] = []) {
    }

    all(): User[] {
        return this.users
    }

    get(id: String): User[] {
        return filter(propEq('id', id), this.users)
    }


}