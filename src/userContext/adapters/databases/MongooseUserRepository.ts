import {MongooseConnection} from "../../configuration/databases/mongoose/mongoose.connection";
import {UserRepository} from "../../domains/repositories/user.repository";
import {User} from "../../domains/entities/user";
import {Users} from './models/users.models';
import {NotFoundUserError} from "../../domains/errors/notFoundUser.error";

export class MongooseUserRepository extends MongooseConnection implements UserRepository {
    all(): User[] {
        return Users.find({}).exec().then((res, err) => {
            if(err) {
                throw new NotFoundUserError('Failed to get users');
            }
            return res;
        });
    };

    get(id: String): User[] {
        return Users.find({_id: id}).exec().then((res, err) => {
            if(err) {
                throw new NotFoundUserError('Failed to get users');
            }
            return res;
        });
    }
}