import { Document, Schema, Model, model} from "mongoose";

export const UserSchema: Schema = new Schema({
    createdAt: Date,
    firstName: String,
    lastName: String,
    email: String
});

UserSchema.pre("save", (next) => {
    const now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
});

export const Users: Model<any> = model<any>("Users", UserSchema);