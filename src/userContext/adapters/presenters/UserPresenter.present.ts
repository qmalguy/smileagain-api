import {User} from "../../domains/entities/user";
import {UserPresented} from "../../domains/viewmodels/userPresented";

export class UserPresenter {
    static present(user: User): UserPresented {
        return {
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email
        }
    }
}