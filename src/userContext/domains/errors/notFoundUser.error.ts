export class NotFoundUserError extends Error {

    constructor(message: string) {
        super(message)
    }

}
