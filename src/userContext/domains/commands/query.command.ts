import {User} from "../entities/user";
import {UserRepository} from "../repositories/user.repository";

export interface QueryCommand {
    query(userRepository: UserRepository): User[]
}