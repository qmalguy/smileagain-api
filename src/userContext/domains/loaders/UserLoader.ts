import {User} from "../entities/user";

export interface UserLoader {
    all(): Array<any>

    get(id: String): Array<User>;
}