export interface UserPresented {
    id: String,
    firstName: String,
    lastName: String,
    email: String
}