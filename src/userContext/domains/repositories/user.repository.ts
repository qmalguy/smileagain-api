import {User} from "../entities/user";

export interface UserRepository {

    all(): User[]

    get(id: String): User[]

}
