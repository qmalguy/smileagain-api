export class User {
    constructor(private _id: String, private _firstName: String, private _lastName: String, private _email: String) {}

    get id(): String {
        return this._id;
    }

    get firstName(): String {
        return this._firstName;
    }

    get lastName(): String {
        return this._lastName;
    }

    get email(): String {
        return this._email;
    }

}
