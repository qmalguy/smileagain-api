import {UserRepository} from "../domains/repositories/user.repository";
import {StubUserBuilder} from "../../../tests/userContext/unit/sutbUser.builder";
import {InMemoryUserRepository} from "../adapters/secondaries/inmemory/InMemoryUser.repository";

export class UserContextDependencyFactory {

    static process: String = process.env.ENV;
    static production: String = 'production';
    static userRepository(): UserRepository {

        if(this.process === UserContextDependencyFactory.production) {
            console.log('Oklm')
            return new InMemoryUserRepository([])
        } else {
            console.log('ERF')
            const Axel = new StubUserBuilder()
                .withId('1')
                .withFirstName('Axel')
                .withLastName('Malguy')
                .withEmail('malguyaxel@gmail.com')
                .build()

            const Quentin = new StubUserBuilder()
                .withId('3')
                .withFirstName('Quentin')
                .withLastName('Malguy')
                .withEmail('malguy.quentin@gmail.com')
                .build()


            return new InMemoryUserRepository([
                Axel,
                Quentin
            ])
        }
        /*switch (this.process) {

            default:
                console.log('proces', process.env.ENV);
                const Axel = new StubUserBuilder()
                    .withId('1')
                    .withFirstName('Axel')
                    .withLastName('Malguy')
                    .withEmail('malguyaxel@gmail.com')
                    .build()

                const Quentin = new StubUserBuilder()
                    .withId('3')
                    .withFirstName('Quentin')
                    .withLastName('Malguy')
                    .withEmail('malguy.quentin@gmail.com')
                    .build()


                return new InMemoryUserRepository([
                    Axel,
                    Quentin
                ])
        }*/
    }
}