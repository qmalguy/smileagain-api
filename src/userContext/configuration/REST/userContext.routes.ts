import {fetchAllUsers} from "../../adapters/primaries/REST/fetchAllUser";

import {userContextDI} from "../userContext.DI";
import {getUserById} from "../../adapters/primaries/REST/getUserById";
const cors = require('cors')
const Raven = require('raven');

Raven.config('https://97ae7553af144cdb9a3a9acd480f8ab7@sentry.io/1244695', {
    logger: 'default'
}).install((err, initialErr, eventId) => {
    console.error(err);
    process.exit(1);
});

export const userContextRoutes = app => {
    app.use(Raven.requestHandler());
    app.options('*', cors())

    app.get("/", (req, res) => res.send("Hello"));
    app.get("/api/v1/users", fetchAllUsers(userContextDI.userHandler));
    app.get("/api/v1/users/:id", getUserById(userContextDI.userHandler));

    app.get("/api/v1/claim", (req, res) => {
        res.send('oklm')
    })

    app.use(Raven.errorHandler());
}
