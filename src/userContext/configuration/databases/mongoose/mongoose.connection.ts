import mongoose from 'mongoose';

export class MongooseConnection {

    public startTransaction() {
        return mongoose.connect('mongodb://localhost/smileagain');
    }

    public endTransaction() {
        return mongoose.connection.close()
    }
}