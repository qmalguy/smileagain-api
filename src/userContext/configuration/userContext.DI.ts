import {UserRepository} from "../domains/repositories/user.repository";
import {UserHandler} from "../usecases/user.handler";
import {UserContextDependencyFactory} from "./userContextDependancyFactory";

const userRepository: UserRepository = UserContextDependencyFactory.userRepository();
const userHandler: UserHandler = new UserHandler(userRepository);

export const userContextDI = {
    userHandler
}
