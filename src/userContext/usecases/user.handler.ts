import {QueryCommand} from "../domains/commands/query.command";
import {UserRepository} from "../domains/repositories/user.repository";

export class UserHandler {
    constructor(private userRepository: UserRepository) {
    }

    query(command: QueryCommand) {
        return command.query(this.userRepository)
    }

}