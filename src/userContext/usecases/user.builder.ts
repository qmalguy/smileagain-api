import {User} from "../domains/entities/user";

export class UserBuilder {
    protected _id: String;
    protected _firstName: String;
    protected _lastName: String;
    protected _email: String;

    withId(value: String): UserBuilder {
        this._id = value
        return this
    }

    withFirstName(value: String): UserBuilder {
        this._firstName = value
        return this
    }

    withLastName(value: String): UserBuilder {
        this._lastName = value
        return this
    }

    withEmail(value: String): UserBuilder {
        this._email = value
        return this
    }

    build(): User {
        return new User (
            this._id,
            this._firstName,
            this._lastName,
            this._email
        )
    }
}
