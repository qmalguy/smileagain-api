import {QueryCommand} from "../../domains/commands/query.command";
import {UserRepository} from "../../domains/repositories/user.repository";
import {User} from "../../domains/entities/user";
import {NotFoundUserError} from "../../domains/errors/notFoundUser.error";

export class GetUserById implements QueryCommand {
    constructor(private id: String) {}

    query(userRepository: UserRepository): User[] {
        const user = userRepository.get(this.id);
        if(user.length <= 0) {
            throw new NotFoundUserError("User " + this.id + " not found")
        }
        return user;
    }

}