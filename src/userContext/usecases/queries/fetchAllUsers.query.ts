import {QueryCommand} from "../../domains/commands/query.command";
import {User} from "../../domains/entities/user";
import {UserRepository} from "../../domains/repositories/user.repository";

export class FetchAllUsers implements QueryCommand {
    query(userRepository: UserRepository): User[] {
        return userRepository.all()
    }
}