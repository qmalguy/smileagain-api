import {userContextRoutes} from '../../userContext/configuration/REST/userContext.routes'

export const rootRoute = app => userContextRoutes(app)