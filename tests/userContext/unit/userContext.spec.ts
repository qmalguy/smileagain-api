import {expect} from 'chai';
import {UserHandler} from "../../../src/userContext/usecases/user.handler";
import {User} from "../../../src/userContext/domains/entities/user";
import {UserRepository} from "../../../src/userContext/domains/repositories/user.repository";
import {FetchAllUsers} from "../../../src/userContext/usecases/queries/fetchAllUsers.query";
import {InMemoryUserRepository} from "../../../src/userContext/adapters/secondaries/inmemory/InMemoryUser.repository";
import {StubUserBuilder} from "./sutbUser.builder";
import {GetUserById} from "../../../src/userContext/usecases/queries/getUserById.query";
import {NotFoundUserError} from "../../../src/userContext/domains/errors/notFoundUser.error";
import {MongooseUserRepository} from "../../../src/userContext/adapters/databases/MongooseUserRepository";
import {MongooseConnection} from "../../../src/userContext/configuration/databases/mongoose/mongoose.connection";

const chai = require('chai');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);

describe('Users handler fetches', () => {

    context('Can fetch a list of pokemons', () => {
        it('With zero user if there is no user in the resource', () => {
            const userHandler: UserHandler = createUserHandler();

            const userList = userHandler.query(new FetchAllUsers());

            expect(userList).deep.equal([])
        })

        it('With some users if there are users in the resource', () => {
            const expectedUserList: User[] = [
                new StubUserBuilder().withFirstName("Quentin").build(),
                new StubUserBuilder().withFirstName("Axel").build()
            ]
            console.log(expectedUserList)
            const userHandler: UserHandler = createUserHandler(expectedUserList);

            const usersList = userHandler.query(new FetchAllUsers());

            verifyUsersList(usersList, expectedUserList);
        })
    });

    context('Can get all information about users', () => {
        it('if the user exist', () => {
            const Lorena = new StubUserBuilder().withFirstName('Lorena').build();
            const Camille = new StubUserBuilder().withFirstName('Camille').withId('2').build();
            const expectedUsersList: User[] = [Lorena, Camille];
            const userHandler: UserHandler = createUserHandler(expectedUsersList);

            const user = userHandler.query(new GetUserById('2'))[0]
            verifyOneUser(user, Camille);
        });

        it('if user is not found throw error', () => {
            const userHandler: UserHandler = createUserHandler();
            const user = () => userHandler.query(new GetUserById('1'));
            expect(user).to.throw("User 1 not found").instanceOf(NotFoundUserError)
        })
    });

    context('Get User from Database', () => {
        it('if the user is valid', async () => {
            const connection = await new MongooseConnection().startTransaction();
            const test = await new MongooseUserRepository().all();
            const test1 = await new MongooseUserRepository().get('5b51c475e09f293ab6c3cc23');
            const stop = await new MongooseConnection().endTransaction();
            console.log(test, test1)
        })
    })
});

function createUserHandler(userPopulation: User[] = []): UserHandler {
    const userRepository: UserRepository = new InMemoryUserRepository(userPopulation);
    return new UserHandler(userRepository);
}

function verifyOneUser(user: User, expectedUser: User) {
    expect(user.id).equal(expectedUser.id)
    expect(user.firstName).equal(expectedUser.firstName)
    expect(user.lastName).equal(expectedUser.lastName)
    expect(user.email).equal(expectedUser.email)
}

function verifyUsersList(userList: User[], expectedUsersList: User[]) {
    expect(userList.length).equal(expectedUsersList.length)
    expectedUsersList.forEach((user, index) => verifyOneUser(user, userList[index]))
}


