import uuidv4 from 'uuid/v4'
import {UserBuilder} from "../../../src/userContext/usecases/user.builder";

export class StubUserBuilder extends UserBuilder {
    protected _id: String = uuidv4();
    protected _firstName: String = "Axel";
    protected _lastName: String = "Malguy";
    protected _email: String = "malguyaxel@gmail.com";
}
